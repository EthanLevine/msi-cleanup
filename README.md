# README #

MSI Cleanup is a small application that cleans up unused files in the Windows Installer directory (C:\Windows\Installer).  On a normal Windows system, this directory can become very large (over 20 GB).  This program scans that directory for installer packages that are no longer referenced by any products or patches and removes them.

Please backup your C:\Windows\Installer directory prior to removing the files.
