﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WindowsInstaller;

namespace MSICleanup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class WizardWindow : Window
    {
        private List<PackageInfo> RegisteredPackages = new List<PackageInfo>();
        private List<string> PackageFiles = new List<string>();
        private List<string> OrphanedPackages = new List<string>();
        private List<PackageInfo> MissingPackages = new List<PackageInfo>();
        private long OrphanedPackagesSize = 0;

        public WizardWindow()
        {
            InitializeComponent();
        }

        private struct PackageInfo
        {
            public string productCode;
            public string patchCode;
            public string location;
        }

        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            // Scan the packages from the WindowsInstaller library and local filesystem
            ScanPackages();

            // Populate fields in the ScanPage
            txbRegisteredPackages.Text = RegisteredPackages.Count.ToString();
            txbPackageFiles.Text = PackageFiles.Count.ToString();
            txbOrphanedPackages.Text = OrphanedPackages.Count.ToString() + " [" + FormatSize(OrphanedPackagesSize) + "]";
            txbMissingPackages.Text = MissingPackages.Count.ToString();

            // Display the ScanPage
            gridIntroPage.Visibility = Visibility.Hidden;
            gridScanPage.Visibility = Visibility.Visible;
        }

        private void btnClean_Click(object sender, RoutedEventArgs e)
        {
            // Get user confirmation for delete
            MessageBoxResult confirmation = MessageBox.Show("THIS WILL DELETE ALL ORPHANED PACKAGES.\n\nYou should backup your C:\\Windows\\Installer folder.\n\nContinue?", "Clean Confirmation", MessageBoxButton.YesNo);
            if (confirmation != MessageBoxResult.Yes)
                return;

            // Delete all orphaned files
            foreach (string file in OrphanedPackages)
            {
                File.Delete(file);
            }

            // Display the CleanPage
            gridScanPage.Visibility = Visibility.Hidden;
            gridCleanPage.Visibility = Visibility.Visible;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private static string FormatSize(long size)
        {
            double convertedSize = size;
            string suffix = "B";

            if (convertedSize > 1024)
            {
                convertedSize /= 1024;
                suffix = "KiB";
            }
            if (convertedSize > 1024)
            {
                convertedSize /= 1024;
                suffix = "MiB";
            }
            if (convertedSize > 1024)
            {
                convertedSize /= 1024;
                suffix = "GiB";
            }

            return string.Format("{0:G3} {1}", convertedSize, suffix);
        }

        private void ScanPackages()
        {
            // Clear any previous scan results
            RegisteredPackages.Clear();
            PackageFiles.Clear();
            OrphanedPackages.Clear();
            MissingPackages.Clear();

            // Get an installer object (requires the WindowsInstaller assembly)
            Type installerType = Type.GetTypeFromProgID("WindowsInstaller.Installer");
            Installer installer = (Installer)Activator.CreateInstance(installerType);

            // Get a list of all registered packages
            foreach (string productCode in installer.Products)
            {
                string productPath = installer.ProductInfo[productCode, "LocalPackage"];
                RegisteredPackages.Add(new PackageInfo { productCode = productCode, patchCode = null, location = productPath });

                // Note: Not all products have patches
                foreach (string patchCode in installer.Patches[productCode])
                {
                    string patchPath = installer.PatchInfo[patchCode, "LocalPackage"];
                    RegisteredPackages.Add(new PackageInfo { productCode = productCode, patchCode = patchCode, location = patchPath });
                }
            }

            // Get a list of all package files
            foreach (string path in Directory.GetFiles(@"C:\Windows\Installer"))
            {
                if (path.EndsWith(".msp") || path.EndsWith(".msi"))
                {
                    PackageFiles.Add(path);
                }
            }

            // Find all package files that do not have a corresponding registered package
            // The hash set contains only lowercase paths
            HashSet<string> registeredPackageSet = new HashSet<string>();
            foreach (PackageInfo pi in RegisteredPackages)
            {
                registeredPackageSet.Add(pi.location.ToLower());
            }
            foreach (string packageFile in PackageFiles)
            {
                if (!registeredPackageSet.Contains(packageFile.ToLower()))
                {
                    OrphanedPackages.Add(packageFile);
                    FileInfo fi = new FileInfo(packageFile);
                    OrphanedPackagesSize += fi.Length;
                }
            }

            // Find all registered packages that are missing package files
            HashSet<string> packageFileSet = new HashSet<string>();
            foreach (string packageFile in PackageFiles)
            {
                packageFileSet.Add(packageFile.ToLower());
            }
            foreach (PackageInfo pi in RegisteredPackages)
            {
                if (!packageFileSet.Contains(pi.location.ToLower()))
                    MissingPackages.Add(pi);
            }
        }
    }
}
